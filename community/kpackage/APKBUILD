# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpackage
pkgver=5.91.0
pkgrel=0
pkgdesc="Framework that lets applications manage user installable packages of non-binary assets"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring installed Plasma, which causes a circular dependency

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
7ad7f0779df403e7fef153a1238b116917b362aaf8aa4abc5cde9a5f4dd1b3baee47498f5d75cf66e22abcd844a55e055041b3454eb28421a6bcf67f3a107ba2  kpackage-5.91.0.tar.xz
"
