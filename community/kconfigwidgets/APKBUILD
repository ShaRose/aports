# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfigwidgets
pkgver=5.91.0
pkgrel=0
pkgdesc="Widgets for KConfig"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kauth-dev
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kguiaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kstandardactiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
58d60aa7fbac6141b0dc4733a22f34cede1944875f5880b0abe743e1637dcc8043cd3e123f4cc1394344bc74b06a673bf7a4d53d01f5c210a36abaa4aa0302e1  kconfigwidgets-5.91.0.tar.xz
"
