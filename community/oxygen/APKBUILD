# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=oxygen
pkgver=5.24.1
pkgrel=0
pkgdesc="Artwork, styles and assets for the Oxygen visual style for the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kcmutils
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	frameworkintegration-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	xcb-util-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/oxygen-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-sounds::noarch"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sounds() {
	pkgdesc="$pkgdesc (sounds)"

	amove usr/share/sounds
}
sha512sums="
0cb88f52309155150f3f3fa1aad3a9dcd35486fd3638e8151db2cdfd813e6e41967ea11efd9b615ef92fd209f407e0dd2b60c006955da681f638ed7a972defd4  oxygen-5.24.1.tar.xz
"
