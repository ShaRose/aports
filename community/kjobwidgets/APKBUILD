# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjobwidgets
pkgver=5.91.0
pkgrel=0
pkgdesc="Widgets for tracking KJob instances"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kcoreaddons-dev
	kwidgetsaddons-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kjobwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a162285b565672c6b950dfd4fe4b68b05f34f399481477c1e8a01868e5a34be2e123e162aa19e9f9c7625bef75d077b16fa051bffe17f9a0590fcbc71ab9461f  kjobwidgets-5.91.0.tar.xz
"
