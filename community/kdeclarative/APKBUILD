# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeclarative
pkgver=5.91.0
pkgrel=0
pkgdesc="Provides integration of QML and KDE Frameworks"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libepoxy-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# quickviewsharedengine requires OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "quickviewsharedengine"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
04a1e321143672489a7441e4fbc5f31ce3508047a0ba582e42632bc51e364086f245df9562b90d8a5a2cf2b31545d8cb862e07f61df30a9d811f6e66ba16d08c  kdeclarative-5.91.0.tar.xz
"
