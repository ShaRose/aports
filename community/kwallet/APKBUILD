# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet
pkgver=5.91.0
pkgrel=0
pkgdesc="Secure and unified container for user passwords"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	gpgme-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	kservice-dev
	kwindowsystem-dev
	libgcrypt-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
provides="dbus:org.freedesktop.Secrets"
# 2nd highst priority after gnome-keyring, mainly for KDE
provider_priority=10

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	local excluded_checks=
	case "$CARCH" in
		s390x) excluded_checks="(blowfishtest)";;
	esac
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$excluded_checks"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
9de1bf99a7772a37f85af6325c4267b42849f1044b4bb536ad5afc979c916d4cc04a8a9b7329f6731d1833ed792a5d772389f470e44637132f6c206a6388bba8  kwallet-5.91.0.tar.xz
"
