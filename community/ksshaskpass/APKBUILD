# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksshaskpass
pkgver=5.24.1
pkgrel=0
pkgdesc="ssh-add helper that uses kwallet and kpassworddialog"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kwallet
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/ksshaskpass-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
da91fab9af11e22384ef20c169827d08c5b3794bf9e8b23fe9db7e305048e32b3836a16c8f2857d52beaeb6f7bdaafe3cb7ac75e9e2e131daa19880083a1a5fd  ksshaskpass-5.24.1.tar.xz
"
