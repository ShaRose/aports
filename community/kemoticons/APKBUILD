# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kemoticons
pkgver=5.91.0
pkgrel=0
pkgdesc="Support for emoticons and emoticons themes"
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kemoticons-kemoticontest and kemoticons-ktexttohtmlplugintest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kemoticons-(kemoticon|ktexttohtmlplugin)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8bd313cd8a7fae78b8bdc500448c5c1870224af594c0626fb7ab9b833793ca9061c2129820bba0378f0b3b616d0ad00cf7b1eae0b75caa04c0e8e4f28baa5d9b  kemoticons-5.91.0.tar.xz
"
