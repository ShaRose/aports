# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=openmp
pkgver=12.0.1
pkgrel=1
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
arch="all !s390x" # LIBOMP_ARCH = UnknownArchitecture
license="Apache-2.0"
options="!check" # 6 tests failing
makedepends="cmake perl elfutils-dev libffi-dev llvm$_llvmver-test-utils samurai"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/openmp-$pkgver.src.tar.xz"
builddir="$srcdir/$pkgname-$pkgver.src"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

sha512sums="
554edf032995cf80cfb6c878b26510b6c4df09e6bd4813934ea523ff8e121900a91ec59c3d83ee0ba390fb83bcaf6d137f7f6019958b444bdfe6a2b35c1c8d08  openmp-12.0.1.src.tar.xz
"
