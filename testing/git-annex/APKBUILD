# Maintainer: Antoine "ayakael" Martin <dev@ayakael.net>
# Contributor: Antoine "ayakael" Martin <dev@ayakael.net>
pkgname=git-annex
pkgver=10.20220127
pkgrel=0
pkgdesc="Manage files with git, without checking their contents into git"
url="http://git-annex.branchable.com"
arch="x86_64"
license="AGPL-3.0-or-later"
depends="
	curl
	git
	openssh-client
	rsync
	"
makedepends="
	alex
	cabal
	dbus-dev
	file-dev
	ghc
	gmp-dev
	gnutls-dev
	happy
	libffi-dev
	libgsasl-dev
	libxml2-dev
	ncurses-dev
	zlib-dev
	"

source="https://git.joeyh.name/index.cgi/git-annex.git/snapshot/git-annex-$pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-zsh-completion"

# Add / remove '-' between "-f" and "FeatureName" to adjust feature build
_feature_flags="
	-fAssistant \
	-fWebApp \
	-fPairing \
	-fProduction \
	-fTorrentParser \
	-fMagicMime \
	-fBenchmark \
	-f-DebugLocks \
	-fDbus \
	-fNetworkBSD \
	-fGitLfs \
	-fHttpClientRestricted \
	"
# The man page is always built but, building the rest of the documentation
# requires ikiwiki. If you want to build the documentation add ikiwiki
# to _cabal_flags
_cabal_flags=" --force-reinstalls "

_cabal_makedepends="c2hs cpphs"
_cabal_libdepends="exceptions hslogger async tasty filepath-bytestring split unix-compat"

_localize_home() {
	ORIG_HOME="$HOME"
	ORIG_TMPDIR="$TMPDIR"
	export HOME="$srcdir"/cabal
	export TMPDIR="$srcdir"/cabal/tmp
	export PATH="$HOME/.cabal/bin:$PATH"
}

_restore_home() {
	export HOME="$ORIG_HOME"
	export TMPDIR="$ORIG_TMPDIR"
}

prepare() {
	default_prepare
	_localize_home
	mkdir -p "$HOME" "$TMPDIR"

	msg "Features: $_feature_flags"

	msg "Installing missing cabal dependencies..."
	cabal update
	cabal install $_cabal_makedepends
	cabal install --lib $_cabal_libdepends

	cabal install $_cabal_flags --user --only-dependencies $_feature_flags
	_restore_home
}

build() {
	_localize_home

	msg "Configuring..."
	cabal configure $_feature_flags

	msg "Starting build..."
	make
	_restore_home
}

check() {
	_localize_home
	make test
	_restore_home
}

package() {
	_localize_home
	make DESTDIR="$pkgdir" install
	_restore_home
}


sha512sums="
b7c1294461d4e0dd0b67a8b7f6795c46ae412dde5cb5345ec21a9abadbcd2843cae0327412ddd781389023548274ca1b97b717b952873a9f0d0830ec443d88cd  git-annex-10.20220127.tar.gz
"
