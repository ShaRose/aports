# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=miniserve
pkgver=0.19.1
pkgrel=0
pkgdesc="Quickly serve files via HTTP"
url="https://github.com/svenstaro/miniserve"
license="MIT"
arch="all !s390x !riscv64" # limited by rust/cargo
arch="$arch !ppc64le" # FTBFS
makedepends="cargo"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="https://github.com/svenstaro/miniserve/archive/v$pkgver/miniserve-$pkgver.tar.gz"

build() {
	cargo build --release --locked

	./target/release/miniserve --print-completions bash > $pkgname.bash
	./target/release/miniserve --print-completions fish > $pkgname.fish
	./target/release/miniserve --print-completions zsh > $pkgname.zsh
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/miniserve "$pkgdir"/usr/bin/miniserve

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
2bd7bda7557233faa421d2525bd4ab400828804882b9cfc7266ff4113fb75524e583fa5cfe19a5f49b04c45e723d1ba06640d5b1074bbec648b8b8265fc8f125  miniserve-0.19.1.tar.gz
"
