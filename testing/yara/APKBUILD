# Maintainer: Daniel Isaksen <d@duniel.no>
pkgname=yara
pkgver=4.1.3
pkgrel=0
pkgdesc="The pattern matching swiss knife for malware researchers"
url="https://virustotal.github.io/yara/"
arch="all !armhf !armv7"  # armhf: tests fail
license="BSD-3-Clause"
makedepends="automake file-dev openssl-dev autoconf libtool flex"
source="$pkgname-$pkgver.tar.gz::https://github.com/VirusTotal/yara/archive/v$pkgver.tar.gz
remove-test-rules.patch"
subpackages="$pkgname-dev $pkgname-doc"

prepare() {
	default_prepare
	autoreconf -fiv
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-crypto \
		--enable-magic
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm 644 COPYING "$pkgdir/usr/share/licenses/$pkgname"/COPYING
	install -Dm 644 README.md "$pkgdir/usr/share/doc/$pkgname"/README.md
	cp -r docs "$pkgdir/usr/share/doc/$pkgname"
}

sha512sums="
1bfa1787c62dfd9a87fa8db5e8c2fa68f082ae66b16b5373bdcc6bc66b32016fcaffd4baa7e59a7c1f6d3426c972eca9cc22f70d475067d7557b1014a4ab65fc  yara-4.1.3.tar.gz
dd3de272614fdfe2ee4658eacacdc54213af44879c1756f2432294221f5fba23e8de028da43f1d47e69c9209266f7d49a6619d7e1af9ef71bc14d07549ad3df1  remove-test-rules.patch
"
